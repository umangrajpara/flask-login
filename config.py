import os

class Config(object):
    # WTForms use this key to protect from CSRF attacks
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'

    # MAIL_SERVER = 'smtp.googlemail.com'
    # MAIL_PORT = 587
    # MAIL_USE_TLS = 1
    # MAIL_USERNAME = 'awesome.empower@gmail.com'
    # MAIL_PASSWORD = 'awesomeempower@1307'
    # ADMINS = ['awesome.empower@gmail.com']

    # GOOGLE_CLIENT_ID = os.environ.get("68307030908-ck3chluftprultj324mjs4flkova8l13.apps.googleusercontent.com", None)
    # GOOGLE_CLIENT_SECRET = os.environ.get("7uxXKgcoIfV1IcWWM2GPqhlA", None)
    # GOOGLE_DISCOVERY_URL = (
    #     "https://accounts.google.com/.well-known/openid-configuration"
    # )

    GOOGLE_CLIENT_ID = ""
    GOOGLE_CLIENT_SECRET = ""
    GOOGLE_DISCOVERY_URL = (
        "https://accounts.google.com/.well-known/openid-configuration"
    )
